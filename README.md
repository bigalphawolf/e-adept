# e-adept-Web-App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.6.

Ce projet constitue la partie cliente de la plateforme de l'ADEPT (Association des Anciens de l'Ecole Polytechniaue de Thies).

Il fait suite a un besoin exprimé par les membres de l'association, d'une application capable de combler un gap certain sur le plan digital.

Ainsi donc un backlog a été rédigé avec des propositions de tout un chacun. Sachant que ce backlog ne saurait être exhaustif et sera complété au fur
et à mesure du process de développement.

Une méthodologie de travail incrémentale c'est à dire par Sprint pourra être utilisé permettant à tout un chacun d'avoir une vue sur la progression.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
